# CI Templates

This repository contains a set of [GitLab CI Templates](https://docs.gitlab.com/ee/development/cicd/templates.html), which can be reused for the [GitLab CI](https://docs.gitlab.com/ee/ci/) of different projects.

Templates are divided into job templates and pipeline templates. While job templates define a single job, pipeline templates contain the combination of multiple jobs into a pipeline. In addition, this repo contains scripts that can be reused or are already used as part of different templates.

This repository works hand in hand with the [CI Examples repository](https://gitlab.com/empaia/integration/ci-examples), which mainly uses the pipeline template [`pipeline-poetry-standard.yml`](pipeline-templates/pipeline-poetry-standard.yml) to access all job templates and thus define its [`.gitlab-ci.yml`](https://gitlab.com/empaia/integration/ci-examples/-/blob/main/.gitlab-ci.yml). It can be seen as a test for all job templates created for web services based on a tech stack of python, poetry and docker.

For example, the pipeline template [`pipeline-poetry-standard.yml`](pipeline-templates/pipeline-poetry-standard.yml) can be easily used in the `.gitlab-ci.yml` of a new service project as follows:

```yaml
include:
  - project: "empaia/integration/ci-templates"
    ref: "main"
    file:
      - "pipeline-templates/pipeline-poetry-standard.yml"

variables:
  TITLE: "Dummy Service"
  DESCRIPTION: "Dummy Service"
  DIRECTORY: "dummy_service"
  TEST_DIRECTORY: "dummy_service/tests"
  RUNNER_TAG: "shared"
  RUNNER_DOCKER_TAG: "shared"
  HOST_PORT: 8000
  CONTAINER_PORT: 8000
  DOCKER_RUN_ALIVE_TEST: "false"
  DOCKER_COMPOSE_ALIVE_TEST: "false"
  POETRY_PYTEST: "true"
  POETRY_PYTEST_COMPOSE: "false"
```

To configure the pipeline, a number of variables are available. Their usage can be explained, if not evident from the name, by searching the job and pipeline templates.

If a pipeline template is used, it is also possible to disable individual jobs as follows:

```yaml
some-pipeline-job:
  rules:
    - when: never
```
