#!/bin/bash
DOCKER_IMAGE=$1
echo "DOCKER IMAGE ${DOCKER_IMAGE}"

IGNORE_PACKAGE_1=$2
IGNORE_PACKAGE_2=${IGNORE_PACKAGE_1//_/-}
echo "IGNORE ${IGNORE_PACKAGE_1} ${IGNORE_PACKAGE_2}"

LICENSE_ALLOW="' \
Apache Software License; \
Apache 2.0; \
BSD; \
BSD License; \
CMU License (MIT-CMU); \
GNU Library or Lesser General Public License (LGPL); \
GNU Lesser General Public License v2 (LGPLv2); \
GNU Lesser General Public License, version 2.1; \
Historical Permission Notice and Disclaimer (HPND); \
ISC; \
ISC License (ISCL); \
MIT; \
MIT License; \
Mozilla Public License 2.0 (MPL 2.0); \
Python Software Foundation License; \
Public Domain; \
The Unlicense (Unlicense); \
Python License (CNRI Python License); \
MIT-CMU; \
UNKNOWN; \
'"

COMMAND=" \
pip3 install pip-licenses; \
pip-licenses \
--ignore-packages pkg-resources pkg_resources ${IGNORE_PACKAGE_1} ${IGNORE_PACKAGE_2}; \
pip-licenses \
--ignore-packages pkg-resources pkg_resources ${IGNORE_PACKAGE_1} ${IGNORE_PACKAGE_2} \
--allow-only=${LICENSE_ALLOW}"

docker run --pull=always --entrypoint="" --rm -u 0 "$DOCKER_IMAGE" /bin/sh -c "$COMMAND"