#!/bin/bash
until
    curl -f -s "$1"
do
    sleep 1; 
    echo -n ".";
done;